package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"golang.org/x/crypto/bcrypt"
)

var db *gorm.DB
var err error

// model user
type Users struct {
	ID           int    `form:"id" json:"id" `
	Username     string `form:"username" json:"username" validate:"required"`
	Password     string `form:"password" json:"password" validate:"required"`
	Nama_Lengkap string `form:"nama_lengkap" json:"nama_lengkap" validate:"required"`
	Foto         string `form:"foto" json:"foto"`
}

func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

// Result is array
type Result struct {
	Code    int         `json:"code"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

func main() {
	db, err = gorm.Open("mysql", "root:@/db_case_go?charset=utf8&parseTime=True")

	if err != nil {
		log.Println("Connection failed", err)
	} else {
		log.Println("Connection established")
	}

	db.AutoMigrate(&Users{})
	handleRequests()
}
func handleRequests() {
	log.Println("server run http://127.0.0.1:3000")

	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)

		res := Result{Code: 404, Message: "Method not found"}
		response, _ := json.Marshal(res)
		w.Write(response)
	})

	myRouter.MethodNotAllowedHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusMethodNotAllowed)

		res := Result{Code: 403, Message: "Method not allowed"}
		response, _ := json.Marshal(res)
		w.Write(response)
	})

	// login handlers
	login := myRouter.Methods(http.MethodPost).Subrouter()
	login.HandleFunc("/", getToken)
	login.HandleFunc("/login", getToken).Methods("POST")

	// user handlers
	// user := myRouter.Methods(http.MethodPost).Subrouter()
	user := myRouter.Methods(http.MethodGet, http.MethodPost, http.MethodPut, http.MethodDelete).Subrouter()
	user.HandleFunc("/api/users", createUser).Methods("POST")
	user.HandleFunc("/api/users", getUsers)
	user.HandleFunc("/api/users/{id}", updateUser).Methods("PUT")
	user.HandleFunc("/api/users/{id}", deleteUsers).Methods("DELETE")

	user.Use(Middleware)
	log.Fatal(http.ListenAndServe(":3000", myRouter))
}
func MainPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome Tes Case Go ... !")
}

// create user
func createUser(w http.ResponseWriter, r *http.Request) {
	payloads, _ := ioutil.ReadAll(r.Body)
	var users Users

	json.Unmarshal(payloads, &users)
	db.Create(&users)

	res := Result{Code: 200, Data: users, Message: "Success create user"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

// Read user
func getUsers(w http.ResponseWriter, r *http.Request) {
	users := []Users{}
	db.Find(&users)

	res := Result{Code: 200, Data: users, Message: "Success get users"}
	results, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(results)
}

// update user
func updateUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["id"]

	payloads, _ := ioutil.ReadAll(r.Body)

	var userUpdateds Users
	json.Unmarshal(payloads, &userUpdateds)

	var users Users
	db.First(&users, userID)
	db.Model(&users).Updates(userUpdateds)

	res := Result{Code: 200, Data: users, Message: "Success update users"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

// delete user
func deleteUsers(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["id"]

	var users Users

	db.First(&users, userID)
	db.Delete(&users)

	res := Result{Code: 200, Message: "Success delete users"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

func getToken(w http.ResponseWriter, r *http.Request) {
	//var user Credential
	sign := jwt.New(jwt.GetSigningMethod("HS256"))
	token, err := sign.SignedString([]byte("secret"))
	data := make(map[string]string)
	data["token"] = token
	res := Result{Code: 200, Data: data, Message: "Success get token"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)

}

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization")
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if jwt.GetSigningMethod("HS256") != token.Method {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			return []byte("secret"), nil
		})

		if token != nil && err == nil {
			h.ServeHTTP(w, r)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusMethodNotAllowed)

			res := Result{Code: 401, Message: "not authorized"}
			response, _ := json.Marshal(res)
			w.Write(response)
		}
	})
}
